package org.fenixsoft.jvm.chapterXX;

/**
 * Created by allenjsl on 2020/11/30.
 */
public class RefCountGC{
    //这个成员属性唯⼀的作⽤就是占⽤⼀点内存
    private byte[] bigSize=new byte[5*1024*1024];//5MB
    Object reference=null;
    public static void main(String[] args) {
        RefCountGC obj1 =new RefCountGC();
        RefCountGC obj2 =new RefCountGC();
        obj1.reference=obj2;
        obj2.reference=obj1;
        obj1 =null;
        obj2 =null;
        //显式的执⾏垃圾回收⾏为
        //这⾥发⽣GC， obj 1和obj 2能否被回收?
        System.gc();
    }
}
